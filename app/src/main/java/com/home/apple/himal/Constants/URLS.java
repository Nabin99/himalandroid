package com.home.apple.himal.Constants;

/**
 * Created by apple on 10/23/16.
 */
public class URLS {

    public static final String MAIN_URL = "http://www.pagodalabs.com.np/himal-ar/",
        GET_PLACES = MAIN_URL + "api/getPlaces",
        IMAGE_PATH = MAIN_URL + "assets/images/";
}
