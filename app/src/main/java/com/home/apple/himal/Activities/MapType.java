package com.home.apple.himal.Activities;

/**
 * Created by apple on 10/26/16.
 */
public enum MapType {
    STANDARD,
    SATELLITE,
    HYBRID
}
