package com.home.apple.himal.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;

import com.home.apple.himal.Constants.SharedPrefKey;
import com.home.apple.himal.R;
import com.home.apple.himal.SupportClasses.SharedPref;

public class InfoActivity extends AppCompatActivity {

    private Button gotItButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        gotItButton = (Button) findViewById(R.id.gotItButton);
        gotItButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //set boolean of info read to true so that this activity is not shown again
                SharedPref.saveToPreferences(InfoActivity.this, SharedPrefKey.fileName,"infoRead",String.valueOf(true));
                setDefaultSettings();
                Intent intent = new Intent(InfoActivity.this,SplashActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }

    public void setDefaultSettings(){
        SharedPref.saveToPreferences(this, SharedPrefKey.fileName,"liveView",String.valueOf(true));
        SharedPref.saveToPreferences(this, SharedPrefKey.fileName, "mapType", String.valueOf(MapType.STANDARD));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
