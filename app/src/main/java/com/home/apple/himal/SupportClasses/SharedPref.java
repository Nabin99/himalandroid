package com.home.apple.himal.SupportClasses;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by apple on 10/23/16.
 */
public class SharedPref {

    public static void saveToPreferences(Context context, String filename, String preferenceName,String preferenceValue){
        SharedPreferences sharedPreferences = context.getSharedPreferences(filename,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(preferenceName,preferenceValue);
        editor.apply();
    }

    public static String readFromPreferences(Context context, String filename, String preferenceName, String defaultValue){
        SharedPreferences sharedPreferences = context.getSharedPreferences(filename, Context.MODE_PRIVATE);
        return sharedPreferences.getString(preferenceName,defaultValue);
    }
}
