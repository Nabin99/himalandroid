package com.home.apple.himal.SupportClasses;

import android.app.Application;
import android.content.Context;

/**
 * Created by apple on 10/23/16.
 */
public class HimalApplication extends Application {
    private static HimalApplication sInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
    }

    public static HimalApplication getInstance(){
        return sInstance;
    }

    public static Context getAppContext(){
        return sInstance.getApplicationContext();
    }
}
