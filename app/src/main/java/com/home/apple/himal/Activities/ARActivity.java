package com.home.apple.himal.Activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.beyondar.android.fragment.BeyondarFragmentSupport;
import com.beyondar.android.plugin.radar.RadarView;
import com.beyondar.android.plugin.radar.RadarWorldPlugin;
import com.beyondar.android.util.ImageUtils;
import com.beyondar.android.view.OnClickBeyondarObjectListener;
import com.beyondar.android.world.BeyondarObject;
import com.beyondar.android.world.BeyondarObjectList;
import com.beyondar.android.world.GeoObject;
import com.beyondar.android.world.World;
import com.google.android.gms.location.places.Place;
import com.home.apple.himal.Constants.URLS;
import com.home.apple.himal.Models.Places;
import com.home.apple.himal.R;
import com.home.apple.himal.Singleton.VolleySingleton;
import com.home.apple.himal.SupportClasses.CustomWorldHelper;

import org.w3c.dom.Text;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class ARActivity extends AppCompatActivity implements OnClickBeyondarObjectListener{

    private BeyondarFragmentSupport mBeyondarFragment;
    private ArrayList<Places> placesArrayList;
    private Location myyLocation;
    private ImageButton settingsBtn;
    private HashMap<BeyondarObject,Places> beyondARPlace;

    private View customView;
    private ImageView imageView;
    private TextView titleView;
    private WebView descriptionView;
    private ProgressBar progressBar;
    private Button knowMoreButton;
    private Dialog dialog;
    private ImageLoader imageLoader;
    private VolleySingleton volleySingleton;
    private Bitmap imageBitmap;
    private File file;
    private File storageDir;

    private static final String TMP_IMAGE_PREFIX = "viewImage_";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // The first thing that we do is to remove all the generated temporal
        // images. Remember that the application needs external storage write
        // permission.
        cleanTempFolder();
        setContentView(R.layout.activity_ar);
        volleySingleton = VolleySingleton.getInstance();
        imageLoader = volleySingleton.getImageLoader();

        setFileForStorage();
        inflateCustomView();

        placesArrayList = getIntent().getParcelableArrayListExtra("placesList");
        myyLocation = getIntent().getParcelableExtra("myLocation");
        mBeyondarFragment = (BeyondarFragmentSupport) getSupportFragmentManager().findFragmentById(R.id.beyondarFragment);
        mBeyondarFragment.setMaxDistanceToRender(10000000);
        mBeyondarFragment.setPullCloserDistance(50);
        mBeyondarFragment.setOnClickBeyondarObjectListener(this);
        settingsBtn = (ImageButton) findViewById(R.id.settings);
        settingsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ARActivity.this,SettingsActivity.class);
                startActivity(intent);
            }
        });
        createARWorld();
    }

    public void createARWorld(){
        World world = new World(this);
        world.setGeoPosition(myyLocation.getLatitude(), myyLocation.getLongitude());
        world.setDefaultImage(R.drawable.settings);
        Log.d("GeoObject:", "" + placesArrayList.size());
        beyondARPlace = new HashMap<>();
        for(int i=0; i<placesArrayList.size();i++){

            Places places = placesArrayList.get(i);
            //create GeoObjects
            GeoObject geoObject = new GeoObject(i);
            geoObject.setGeoPosition(places.getLocation().getLatitude(), places.getLocation().getLongitude());
            geoObject.setImageResource(R.drawable.settings);
            geoObject.setName(places.getName());
            //add geoobjects to the world
            world.addBeyondarObject(geoObject);
            //Map beyondARObject to custom Class Places
            BeyondarObjectList list = world.getBeyondarObjectList(0);
            BeyondarObject object = list.get(i);
            beyondARPlace.put(object,places);
        }

        mBeyondarFragment.setWorld(world);

        replaceImagesByStaticViews(world);

    }


    public void replaceImagesByStaticViews(World world){

        String path = getTmpPath();
        for(BeyondarObjectList beyondarObjectList: world.getBeyondarObjectLists()){
            Log.d("List of geo:",""+ beyondarObjectList.size());
            for(BeyondarObject beyondarObject: beyondarObjectList){
                //first get the view, inflate it and change some stuff
                Places places = beyondARPlace.get(beyondarObject);
                View view = getLayoutInflater().inflate(R.layout.static_beyondar_object_view,null);
                TextView textView = (TextView) view.findViewById(R.id.geoObjectName);
                textView.setText(places.getName());
                TextView height = (TextView) view.findViewById(R.id.height);
                height.setText(places.getHeight());
                TextView distance = (TextView) view.findViewById(R.id.distance);
                distance.setText(String.valueOf(beyondarObject.getDistanceFromUser()));

                try{
                    // Now that we have it we need to store this view in the
                    // storage in order to allow the framework to load it when
                    // it will be need it
                    String imageName = TMP_IMAGE_PREFIX + beyondarObject.getName() + ".png";
                    ImageUtils.storeView(view,path,imageName);

                    // If there are no errors we can tell the object to use the
                    // view that we just stored
                    beyondarObject.setImageUri(path + imageName);
                } catch (IOException e){
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Get the path to store temporally the images. Remember that you need to
     * set WRITE_EXTERNAL_STORAGE permission in your manifest in order to
     * write/read the storage
     */
    private String getTmpPath() {
        return getExternalFilesDir(null).getAbsoluteFile() + "/tmp/";
    }

    /** Clean all the generated files */
    private void cleanTempFolder() {
        File tmpFolder = new File(getTmpPath());
        if (tmpFolder.isDirectory()) {
            String[] children = tmpFolder.list();
            for (int i = 0; i < children.length; i++) {
                if (children[i].startsWith(TMP_IMAGE_PREFIX)) {
                    new File(tmpFolder, children[i]).delete();
                }
            }
        }
    }


    @Override
    public void onClickBeyondarObject(ArrayList<BeyondarObject> beyondarObjects) {
        if(beyondarObjects.size() > 0){
            BeyondarObject beyondarObject = beyondarObjects.get(0);
            final Places places = beyondARPlace.get(beyondarObject);

            titleView.setText(places.getName());
            descriptionView.loadData(places.getDesc(), "text", "UTF-8");

            dialog.setTitle("Details");
            setImage(places);
            knowMoreButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setWebLink(places);
                }
            });

            dialog.setCanceledOnTouchOutside(true);
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(dialog.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.gravity = Gravity.BOTTOM;

            dialog.getWindow().setAttributes(lp);

            dialog.show();
        }
    }

    public void inflateCustomView(){
        customView = getLayoutInflater().inflate(R.layout.description_view,null);
        imageView = (ImageView) customView.findViewById(R.id.image);
        titleView = (TextView) customView.findViewById(R.id.title);
        descriptionView = (WebView) customView.findViewById(R.id.description);
        progressBar = (ProgressBar) customView.findViewById(R.id.image_progressBar);
        knowMoreButton = (Button) customView.findViewById(R.id.know_more);
        dialog = new Dialog(this);
        dialog.setContentView(customView);
    }

    public void setImage(final Places places) {

        File imgFile = new File(storageDir + "/" + places.getImageName());
        if (imgFile.exists()) {
            Bitmap imgBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            if (imgBitmap != null) {
                imageView.setImageBitmap(imgBitmap);
            } else {
                imageView.setImageResource(R.drawable.no_image);
            }

        } else {
            if(places.getImageName() != "") {
                progressBar.setVisibility(View.VISIBLE);
                imageLoader.get(URLS.IMAGE_PATH + places.getImageName(), new ImageLoader.ImageListener() {
                    @Override
                    public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                        progressBar.setVisibility(View.INVISIBLE);
                        imageView.setImageBitmap(response.getBitmap());

                        imageBitmap = response.getBitmap();

                        //save Image to DAtabase
                        if (imageBitmap != null) {
                            File imageFile = new File(file, places.getImageName());
                            try {
                                FileOutputStream out = new FileOutputStream(imageFile);
                                imageBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
                                out.flush();
                                out.close();
                            } catch (FileNotFoundException e) {
                                e.printStackTrace();
                                Log.d("Saving error:", " File not Found");
                            } catch (IOException e) {
                                e.printStackTrace();
                                Log.d("Saving error:", " IO Exception");
                            }
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressBar.setVisibility(View.INVISIBLE);
                        imageView.setImageResource(R.drawable.no_image);
                    }
                });
            } else {
                imageView.setImageResource(R.drawable.no_image);
            }
        }
    }

    public void setFileForStorage(){
        storageDir = new File(Environment.getExternalStorageDirectory() + "/himal/");
        file = new File(String.valueOf(storageDir));
        file.mkdirs();
    }

    public void setWebLink(Places places){
        String url = places.getWebLink();
        if(url != ""){
            Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse(url));
            startActivity(intent);
        }
    }
}
