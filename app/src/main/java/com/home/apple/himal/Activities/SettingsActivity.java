package com.home.apple.himal.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;

import com.home.apple.himal.Constants.SharedPrefKey;
import com.home.apple.himal.R;
import com.home.apple.himal.SupportClasses.SharedPref;

public class SettingsActivity extends AppCompatActivity implements Switch.OnCheckedChangeListener{

    private Switch liveViewSwitch;
    private RadioGroup mapTypeGroup;
    private Button saveBtn;
    private Button cancel;
    private Boolean liveView;
    private MapType mapType;
    private RadioButton mapButton;
    private RadioButton standardBtn;
    private RadioButton satelliteBtn;
    private RadioButton hybridBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        liveView = Boolean.valueOf(SharedPref.readFromPreferences(this, SharedPrefKey.fileName,"liveView",""));
        mapType = MapType.valueOf(SharedPref.readFromPreferences(this,SharedPrefKey.fileName,"mapType",""));

        liveViewSwitch = (Switch) findViewById(R.id.liveViewSwitch);
        mapTypeGroup = (RadioGroup) findViewById(R.id.map_group);
        saveBtn = (Button) findViewById(R.id.save_settings);
        cancel = (Button) findViewById(R.id.cancel);
        standardBtn = (RadioButton) findViewById(R.id.standard_map);
        satelliteBtn = (RadioButton) findViewById(R.id.satellite_map);
        hybridBtn = (RadioButton) findViewById(R.id.hybrid_map);


        //set switch
        liveViewSwitch.setChecked(liveView);

        //set map Type
        String type = SharedPref.readFromPreferences(this,SharedPrefKey.fileName,"mapType","");
        if(MapType.valueOf(type) == MapType.SATELLITE){
            satelliteBtn.setChecked(true);
        } else if(MapType.valueOf(type) == MapType.HYBRID){
            hybridBtn.setChecked(true);
        } else {
            standardBtn.setChecked(true);
        }

        liveViewSwitch.setOnCheckedChangeListener(this);

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveSettings();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(isChecked){
            liveView = true;
            Log.d("Live View status:","" + liveView);
        } else {
            liveView = false;
            Log.d("Live View status:","" + liveView);
        }
    }

    public void saveSettings(){
        MapType mapType = MapType.STANDARD;
        int selectedId = mapTypeGroup.getCheckedRadioButtonId();
        mapButton = (RadioButton) findViewById(selectedId);

        switch(mapButton.getText().toString()) {
            case "Standard":
                mapType = MapType.STANDARD;
                break;
            case "Satellite":
                mapType = MapType.SATELLITE;
                break;

            case "Hybrid":
                mapType = MapType.HYBRID;
                break;
        }

        Log.d("Live View status:", "" + liveView + "  " + mapType);

        SharedPref.saveToPreferences(this, SharedPrefKey.fileName, "liveView", String.valueOf(liveView));
        SharedPref.saveToPreferences(this,SharedPrefKey.fileName,"mapType",String.valueOf(mapType));

        Intent intent = new Intent(this,MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }
}
