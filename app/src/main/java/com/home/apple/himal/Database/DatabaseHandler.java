package com.home.apple.himal.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.google.android.gms.location.places.Place;
import com.google.gson.Gson;
import com.home.apple.himal.Models.Places;

import java.util.ArrayList;

/**
 * Created by apple on 10/23/16.
 */
public class DatabaseHandler extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "himal";

    //Table
    private static final String DATABASE_TABLE_PLACES = "placesTable";
    private static final String KEY_PLACE = "places";

    private static final String CREATE_TABLE_PLACES = "CREATE TABLE IF NOT EXISTS " + DATABASE_TABLE_PLACES + "(" + KEY_PLACE + " TEXT)";

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_PLACES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE " + DATABASE_TABLE_PLACES);
        onCreate(db);
    }

    public DatabaseHandler(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void savePlaces(String places){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_PLACE,places);
        db.insert(DATABASE_TABLE_PLACES, null, values);
    }

    public ArrayList<Places> getPlaces(){
        ArrayList<Places> placesArrayList = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from " + DATABASE_TABLE_PLACES,null);
        res.moveToFirst();

        Log.d("Before While:", "Before while");
        while(!res.isAfterLast()){
            int key_place = res.getColumnIndex(KEY_PLACE);
            String places = res.getString(key_place);
            Gson gson = new Gson();
            Places places1 = gson.fromJson(places,Places.class);
            Log.d("After While:","" + places1.getName());
            placesArrayList.add(places1);
            res.moveToNext();
        }
        return placesArrayList;
    }

    public void clearTable(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM "+ DATABASE_TABLE_PLACES);
    }
}
