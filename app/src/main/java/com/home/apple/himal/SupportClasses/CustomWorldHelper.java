package com.home.apple.himal.SupportClasses;

import android.content.Context;
import android.util.Log;

import com.beyondar.android.world.GeoObject;
import com.beyondar.android.world.World;
import com.home.apple.himal.Models.Places;
import com.home.apple.himal.R;

import java.util.ArrayList;

/**
 * Created by apple on 10/24/16.
 */
public class CustomWorldHelper {

    private Context context;
    private ArrayList<Places> placesArrayList;

    public World sharedWorld;

    public CustomWorldHelper(Context context, ArrayList<Places> placesArrayList){
        this.context = context;
        this.placesArrayList = placesArrayList;
    }

    public World generateObjects(){
//        if(sharedWorld != null){
//            return sharedWorld;
//        }
        sharedWorld = new World(context);
        sharedWorld.setGeoPosition(27.682910,85.305503);
        Log.d("World list:", "" + placesArrayList.size());
        for (int i=0;i<placesArrayList.size();i++){
            Places places = placesArrayList.get(i);

            GeoObject geoObject = new GeoObject(i);
            geoObject.setGeoPosition(places.getLocation().getLatitude(),places.getLocation().getLongitude());
            geoObject.setImageResource(R.drawable.settings);
            geoObject.setName(places.getName());

            sharedWorld.addBeyondarObject(geoObject);
        }
        return sharedWorld;
    }
}
