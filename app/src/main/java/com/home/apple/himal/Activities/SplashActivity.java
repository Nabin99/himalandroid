package com.home.apple.himal.Activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.home.apple.himal.Constants.SharedPrefKey;
import com.home.apple.himal.SupportClasses.PlacesLoader;
import com.home.apple.himal.SupportClasses.SharedPref;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String infoRead = SharedPref.readFromPreferences(this,SharedPrefKey.fileName,"infoRead","");

        if(Boolean.valueOf(infoRead)){
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        } else {
            Intent intent = new Intent(this, InfoActivity.class);
            startActivity(intent);
            finish();
        }
    }
}
