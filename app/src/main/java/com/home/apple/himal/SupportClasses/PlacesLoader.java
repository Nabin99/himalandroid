package com.home.apple.himal.SupportClasses;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.home.apple.himal.Activities.ARActivity;
import com.home.apple.himal.Activities.MainActivity;
import com.home.apple.himal.Activities.MapActivity;
import com.home.apple.himal.Constants.SharedPrefKey;
import com.home.apple.himal.Constants.URLS;
import com.home.apple.himal.Database.DatabaseHandler;
import com.home.apple.himal.Models.Places;
import com.home.apple.himal.Singleton.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by apple on 10/23/16.
 */
public class PlacesLoader {

    private Context context;
    private Location myLocation;
    private ArrayList<Places> placesArrayList;

    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;
    private ProgressDialog progressDialog;
    private DatabaseHandler databaseHandler;
    private ProgressBar progressBar;

    public PlacesLoader(Context context, Location myLocation,ProgressBar progressBar){
        this.context = context;
        this.myLocation = myLocation;
        this.progressBar = progressBar;
        volleySingleton = VolleySingleton.getInstance();
        requestQueue = volleySingleton.getRequestQueue();
    }

    public void getPlaces(){
        databaseHandler = new DatabaseHandler(context);
        //get all the places saved in sqlitedatabase
        ArrayList<Places> list = databaseHandler.getPlaces();
      //if places are available show in map and check for updates
        if(list.size() > 0){
            checkForUpdates();
            showAnotherActivity(list);
        } else {
            //progressBar.setVisibility(View.INVISIBLE);
            //get places from server
            getDataFromServer();
        }
    }

    public void getDataFromServer(){

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET,
                URLS.GET_PLACES,
                new Response.Listener<JSONObject>() {

                    @Override

                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();
                       parseJSONResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        Toast.makeText(context, VolleyErrorMessage.handleVolleyErrors(context,error), Toast.LENGTH_SHORT).show();
                    }
                });
        requestQueue.add(request);
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Please Wait....");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    public void parseJSONResponse(JSONObject response){
        databaseHandler.clearTable();
        placesArrayList = new ArrayList<>();
        try {
            JSONArray dateArray = response.getJSONArray("version");
            String updatedDate = dateArray.getJSONObject(0).getString("info_version");

            Date recentUpdatedDate = convertStringToDate(updatedDate);
            Log.d("Update time:","" + recentUpdatedDate);
            long dateinMillis = recentUpdatedDate.getTime();
            Log.d("Update time in millis:","" + dateinMillis);

            SharedPref.saveToPreferences(context, SharedPrefKey.fileName, "pref_updatedTime", String.valueOf(dateinMillis));

            JSONArray details = response.getJSONArray("data");
            for(int i=0;i<details.length();i++){

                Places places = new Places();

                JSONObject detailObject = details.getJSONObject(i);
                Double latitude = detailObject.getDouble("latitude");
                Double longitude = detailObject.getDouble("longitude");
                String name = detailObject.optString("title");
                String type = detailObject.optString("type");
                String imageName = detailObject.optString("image");
                Integer id = detailObject.getInt("id");
                String description = detailObject.optString("description");
                String url = detailObject.optString("url");
                String height = detailObject.optString("height");

                Location location = new Location("");
                location.setLatitude(latitude);
                location.setLongitude(longitude);

                places.setId(id);
                places.setName(name);
                places.setLocation(location);
                places.setDesc(description);
                places.setType(type);
                places.setImageName(imageName);
                places.setWebLink(url);
                places.setHeight(height);

                Gson gson = new Gson();
                String place = gson.toJson(places);
                Log.d("Place before saving:","" + place);
                databaseHandler.savePlaces(place);
                placesArrayList.add(places);
            }
            //addDefaultSettings();
            showAnotherActivity(placesArrayList);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public Date convertStringToDate(String dateString){
        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try{
            date = format.parse(dateString);
        }catch (ParseException e){
            e.printStackTrace();
        }
        return date;
    }

//    public void showMapViewActivity(ArrayList<Places> listOfPlaces){
//            Bundle bundle = new Bundle();
//            bundle.putParcelableArrayList("placesList", listOfPlaces);
//
//            Intent intent = new Intent(context, MapActivity.class);
//            intent.putExtras(bundle);
//            context.startActivity(intent);
//            ((MainActivity) context).finish();
//    }
//
//    public void showARActivity(ArrayList<Places> listOfPlaces){
//         Bundle bundle = new Bundle();
//        bundle.putParcelableArrayList("placesList",listOfPlaces);
//        bundle.putParcelable("myLocation", myLocation);
//        Intent intent = new Intent(context, ARActivity.class);
//        intent.putExtras(bundle);
//        context.startActivity(intent);
//        ((MainActivity) context).finish();
//    }



    public void showAnotherActivity(ArrayList<Places> listOfPlaces){
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("placesList",listOfPlaces);
        bundle.putParcelable("myLocation", myLocation);

        String liveView = SharedPref.readFromPreferences(context,SharedPrefKey.fileName,"liveView","");
        String mapType = SharedPref.readFromPreferences(context,SharedPrefKey.fileName,"mapType","");

        Log.d("Live View on Main:","" + liveView + " " + mapType);

        if(Boolean.valueOf(liveView)){
            Intent intent = new Intent(context, ARActivity.class);
            intent.putExtras(bundle);
            context.startActivity(intent);
            ((MainActivity) context).finish();
        } else {
            Intent intent = new Intent(context, MapActivity.class);
            intent.putExtras(bundle);
            context.startActivity(intent);
            ((MainActivity) context).finish();
        }


    }

    public void checkForUpdates(){
        new Thread(new Runnable(){

            @Override
            public void run() {
                updateData();
            }
        }).start();
    }

    public void updateData(){
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET,
                URLS.GET_PLACES,
                new Response.Listener<JSONObject>() {

                    @Override

                    public void onResponse(JSONObject response) {
                        parseJSONToUpdate(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                });
        requestQueue.add(request);
    }

    public void parseJSONToUpdate(JSONObject response){
        try {
            JSONArray dateArray = response.getJSONArray("version");
            String updatedDate = dateArray.getJSONObject(0).getString("info_version");

            Date recentUpdatedDate = convertStringToDate(updatedDate);
            Log.d("Update time:","" + recentUpdatedDate);
            long dateinMillis = recentUpdatedDate.getTime();
            Log.d("Update time in millis:","" + dateinMillis);

            String lastUpdated = SharedPref.readFromPreferences(context,SharedPrefKey.fileName,"pref_updatedTime","");
            if(!lastUpdated.equals(String.valueOf(dateinMillis))){
                //Clear Whole table
                databaseHandler.clearTable();
                //save the updatedTime
                SharedPref.saveToPreferences(context, SharedPrefKey.fileName, "pref_updatedTime", String.valueOf(dateinMillis));

                JSONArray details = response.getJSONArray("data");
                for(int i=0;i<details.length();i++){

                    Places places = new Places();

                    JSONObject detailObject = details.getJSONObject(i);
                    Double latitude = detailObject.getDouble("latitude");
                    Double longitude = detailObject.getDouble("longitude");
                    String name = detailObject.optString("title");
                    String type = detailObject.optString("type");
                    String imageName = detailObject.optString("image");
                    Integer id = detailObject.getInt("id");
                    String description = detailObject.optString("description");
                    String url = detailObject.optString("url");
                    String height = detailObject.optString("height");

                    Location location = new Location("");
                    location.setLatitude(latitude);
                    location.setLongitude(longitude);

                    places.setId(id);
                    places.setName(name);
                    places.setLocation(location);
                    places.setDesc(description);
                    places.setType(type);
                    places.setImageName(imageName);
                    places.setWebLink(url);
                    places.setHeight(height);

                    Gson gson = new Gson();
                    String place = gson.toJson(places);
                    Log.d("Place before saving:", "" + place);
                    databaseHandler.savePlaces(place);
                }
            } else {
                Log.d("Saved Data:","No new DAta");
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
