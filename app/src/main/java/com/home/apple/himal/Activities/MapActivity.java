package com.home.apple.himal.Activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.home.apple.himal.Constants.Key;
import com.home.apple.himal.Constants.SharedPrefKey;
import com.home.apple.himal.Constants.URLS;
import com.home.apple.himal.Models.Places;
import com.home.apple.himal.R;
import com.home.apple.himal.Singleton.VolleySingleton;
import com.home.apple.himal.SupportClasses.SharedPref;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback,GoogleMap.OnMarkerClickListener{

    private GoogleMap mapView;
    private SupportMapFragment mapFragment;
    private HashMap<Marker,Places> placeMarker;
    private ArrayList<Places> placesArrayList;
    private ImageButton settingsBtn;
    private Integer mapType;
    private View customView;
    private ImageView imageView;
    private TextView titleView;
    private WebView descriptionView;
    private ProgressBar progressBar;
    private Button knowMoreButton;
    private Dialog dialog;
    private ImageLoader imageLoader;
    private VolleySingleton volleySingleton;
    private Bitmap imageBitmap;
    private File file;
    private File storageDir;
    private Location myLocation;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        volleySingleton = VolleySingleton.getInstance();
        imageLoader = volleySingleton.getImageLoader();
        myLocation = getIntent().getParcelableExtra("myLocation");

        setFileForStorage();
        inflateCustomView();

        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        settingsBtn = (ImageButton) findViewById(R.id.settings);
        placesArrayList = getIntent().getParcelableArrayListExtra("placesList");

        //check for maptype
        checkForMapType();
        mapFragment.getMapAsync(this);

        settingsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MapActivity.this, SettingsActivity.class);
                startActivity(intent);
            }
        });

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(myLocation.getLatitude(),myLocation.getLongitude()), 8));
        googleMap.setMapType(mapType);
        placeMarker = new HashMap<>();
        for(int i=0;i<placesArrayList.size();i++){
            Places places = placesArrayList.get(i);
            Marker marker = googleMap.addMarker(new MarkerOptions().position(new LatLng(places.getLocation().getLatitude(), places.getLocation().getLongitude())) .title(places.getName()));
            placeMarker.put(marker,places);
        }
        googleMap.setOnMarkerClickListener(this);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        final Places places = placeMarker.get(marker);
        titleView.setText(places.getName());
        descriptionView.loadData(places.getDesc(), "text", "UTF-8");

        dialog.setTitle("Details");
        setImage(places);
        knowMoreButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setWebLink(places);
            }
        });

        dialog.setCanceledOnTouchOutside(true);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.BOTTOM;

        dialog.getWindow().setAttributes(lp);

        dialog.show();

        return true;
    }

    public void checkForMapType(){
        String type = SharedPref.readFromPreferences(this, SharedPrefKey.fileName, "mapType", "");
        if(MapType.valueOf(type) == MapType.SATELLITE){
            mapType = GoogleMap.MAP_TYPE_SATELLITE;
        } else if(MapType.valueOf(type) == MapType.HYBRID){
            mapType = GoogleMap.MAP_TYPE_HYBRID;
        } else {
            mapType = GoogleMap.MAP_TYPE_NORMAL;
        }
    }

    public void inflateCustomView(){
        customView = getLayoutInflater().inflate(R.layout.description_view,null);
        imageView = (ImageView) customView.findViewById(R.id.image);
        titleView = (TextView) customView.findViewById(R.id.title);
        descriptionView = (WebView) customView.findViewById(R.id.description);
        progressBar = (ProgressBar) customView.findViewById(R.id.image_progressBar);
        knowMoreButton = (Button) customView.findViewById(R.id.know_more);
        dialog = new Dialog(this);
        dialog.setContentView(customView);
    }

    public void setImage(final Places places) {

        File imgFile = new File(storageDir + "/" + places.getImageName());
        if (imgFile.exists()) {
            Bitmap imgBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            if (imgBitmap != null) {
                imageView.setImageBitmap(imgBitmap);
            } else {
                imageView.setImageResource(R.drawable.no_image);
            }

        } else {
        if(places.getImageName() != "") {
            progressBar.setVisibility(View.VISIBLE);
            imageLoader.get(URLS.IMAGE_PATH + places.getImageName(), new ImageLoader.ImageListener() {
                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                    progressBar.setVisibility(View.INVISIBLE);
                    imageView.setImageBitmap(response.getBitmap());

                    imageBitmap = response.getBitmap();

                    //save Image to DAtabase
                    if (imageBitmap != null) {
                        File imageFile = new File(file, places.getImageName());
                        try {
                            FileOutputStream out = new FileOutputStream(imageFile);
                            imageBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
                            out.flush();
                            out.close();
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                            Log.d("Saving error:", " File not Found");
                        } catch (IOException e) {
                            e.printStackTrace();
                            Log.d("Saving error:", " IO Exception");
                        }
                    }
                }

                @Override
                public void onErrorResponse(VolleyError error) {
                    progressBar.setVisibility(View.INVISIBLE);
                    imageView.setImageResource(R.drawable.no_image);
                }
            });
        } else {
            imageView.setImageResource(R.drawable.no_image);
            }
        }
    }

    public void setFileForStorage(){
        storageDir = new File(Environment.getExternalStorageDirectory() + "/himal/");
        file = new File(String.valueOf(storageDir));
        file.mkdirs();
    }

    public void setWebLink(Places places){
            String url = places.getWebLink();
        if(url != ""){
            Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse(url));
            startActivity(intent);
        }
    }
}
