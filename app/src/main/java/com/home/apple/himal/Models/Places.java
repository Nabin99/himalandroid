package com.home.apple.himal.Models;

import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.design.internal.ParcelableSparseArray;

import java.io.Serializable;

/**
 * Created by apple on 10/23/16.
 */
public class Places implements Parcelable{

    private Integer id;
    private String name;
    private String desc;
    private Location location;
    private String imageName;
    private String webLink;
    private String type;
    private String height;

    public Places() {
    }

    public Places(Integer id, String height, String type, String webLink, String imageName, Location location, String desc, String name) {
        this.id = id;
        this.height = height;
        this.type = type;
        this.webLink = webLink;
        this.imageName = imageName;
        this.location = location;
        this.desc = desc;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getWebLink() {
        return webLink;
    }

    public void setWebLink(String webLink) {
        this.webLink = webLink;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    protected Places(Parcel in) {
        id = in.readByte() == 0x00 ? null : in.readInt();
        name = in.readString();
        desc = in.readString();
        location = (Location) in.readValue(Location.class.getClassLoader());
        imageName = in.readString();
        webLink = in.readString();
        type = in.readString();
        height = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(id);
        }
        dest.writeString(name);
        dest.writeString(desc);
        dest.writeValue(location);
        dest.writeString(imageName);
        dest.writeString(webLink);
        dest.writeString(type);
        dest.writeString(height);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Places> CREATOR = new Parcelable.Creator<Places>() {
        @Override
        public Places createFromParcel(Parcel in) {
            return new Places(in);
        }

        @Override
        public Places[] newArray(int size) {
            return new Places[size];
        }
    };
}
